

window.onload = function(){
    // this function is to allow drop over another element 

    function allowDrop(ev) { 
        ev.preventDefault();
    }

// this is to set data on drag to be carried 

    function drag(ev) {
        ev.dataTransfer.setData("src", ev.target.id);
        console.log("drag");
    }

// action ondrop 

    function drop(ev) {
        ev.preventDefault();
        var src = document.getElementById(ev.dataTransfer.getData("src"));
        var srcParent = src.parentNode;
        var tgt = ev.currentTarget.firstElementChild;
        ev.currentTarget.replaceChild(src, tgt);
        srcParent.appendChild(tgt);
    }


    $('.menu_toggle').click(function() {
        $('.menu_toggle').toggleClass('active');
        $('nav').toggleClass('active');
    });
};



